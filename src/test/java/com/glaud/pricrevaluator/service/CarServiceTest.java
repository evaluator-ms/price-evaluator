package com.glaud.pricrevaluator.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.glaud.priceevaluator.config.RabbitConfig;
import com.glaud.priceevaluator.model.Mark;
import com.glaud.priceevaluator.service.CarService;

@ExtendWith(SpringExtension.class)
public class CarServiceTest {

	private CarService carService;

	@Mock
	RabbitTemplate rabbitTemplate;

	@BeforeEach
	public void setUp() {
		carService = new CarService(rabbitTemplate);
	}

	@Test
	public void getPriceListTest() throws IOException {
		when(rabbitTemplate.convertSendAndReceiveAsType(RabbitConfig.EXCHANGE_MODELS, "rpc.models", "honda",
				new ParameterizedTypeReference<Mark>() {
				})).thenReturn(new Mark.Builder("honda").build());
		Mark mark = carService.getMark("honda");
		assertEquals(mark, new Mark.Builder("honda").build());
	}
}
