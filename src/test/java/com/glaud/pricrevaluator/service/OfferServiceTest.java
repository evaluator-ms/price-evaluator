package com.glaud.pricrevaluator.service;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import com.glaud.priceevaluator.data.Offer;
import com.glaud.priceevaluator.service.DocumentService;
import com.glaud.priceevaluator.service.OtoMotoOfferService;
import com.glaud.priceevaluator.service.OfferService;

@ExtendWith(SpringExtension.class)
public class OfferServiceTest {

	private OfferService otoMotoOfferService;

	@BeforeEach
	public void setUp() {
		otoMotoOfferService = new OtoMotoOfferService(new DocumentService());
		ReflectionTestUtils.setField(otoMotoOfferService, "url", "https://www.otomoto.pl/osobowe/");
	}

	@Test
	public void getPriceListTest() throws IOException {
		String mark = "audi";
		String model = "q3";
		List<Offer> offers = otoMotoOfferService.getOfferList(mark, model);
		double offersSum = offers.stream().mapToDouble(offer -> offer.getPrice().getValue()).sum();
		assertTrue(offersSum > 100000);
	}


}
