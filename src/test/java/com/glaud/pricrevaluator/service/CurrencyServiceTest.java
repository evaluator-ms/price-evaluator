package com.glaud.pricrevaluator.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.glaud.priceevaluator.data.CurrencyRates;
import com.glaud.priceevaluator.data.Price;
import com.glaud.priceevaluator.model.Currency;
import com.glaud.priceevaluator.service.CurrencyService;

@ExtendWith(SpringExtension.class)
public class CurrencyServiceTest {

	private CurrencyService currencyService;
	@Mock
	private CurrencyRates currencyRates;

	@BeforeEach
	public void setUp() {
		currencyService = new CurrencyService(currencyRates);
	}

	@Test
	public void convertToPlnTest() {
		when(currencyRates.getCurrencyRate(Currency.EUR)).thenReturn(0.23);
		double toConvert = 10;
		double actual = currencyService.convertPriceToPln(new Price(toConvert, Currency.EUR));
		assertEquals(10 / 0.23, actual);
	}

}
