package com.glaud.pricrevaluator.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import static org.mockito.Mockito.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.glaud.priceevaluator.PriceEvaluatorApplication;
import com.glaud.priceevaluator.data.Offer;
import com.glaud.priceevaluator.data.Price;
import com.glaud.priceevaluator.data.Statistics;
import com.glaud.priceevaluator.model.Currency;
import com.glaud.priceevaluator.service.CurrencyService;
import com.glaud.priceevaluator.service.OfferService;
import com.glaud.priceevaluator.service.StatisticsService;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = PriceEvaluatorApplication.class)
public class StatisticsServiceTest {

	private StatisticsService statisticsService;

	@Mock
	private OfferService offerService;

	@Autowired
	CurrencyService currencyService;;

	@BeforeEach
	public void setUp() {
		statisticsService = new StatisticsService(offerService, currencyService);

	}

	@Test
	public void computeStatisticsTest() throws IOException {
		Offer offer1 = new Offer.Builder(new Price(new Double(10), Currency.PLN)).build();
		Offer offer2 = new Offer.Builder(new Price(new Double(20.5), Currency.PLN)).build();
		Offer offer3 = new Offer.Builder(new Price(new Double(29.5), Currency.PLN)).build();
		when(offerService.getOfferList("audi", "a3")).thenReturn(Arrays.asList(offer1, offer2, offer3));
		Statistics actualStatistics = statisticsService.computeStatistics("audi", "a3");
		Statistics expectedStatistics = new Statistics();
		expectedStatistics.setCount(3);
		expectedStatistics.setAvg(new Double(20));
		expectedStatistics.setMin(new Double(10));
		expectedStatistics.setMax(new Double(29.5));
		assertEquals(expectedStatistics, actualStatistics);
	}

}
