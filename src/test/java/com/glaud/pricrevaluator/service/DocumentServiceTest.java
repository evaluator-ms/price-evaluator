package com.glaud.pricrevaluator.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;

import org.jsoup.nodes.Document;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.glaud.priceevaluator.service.DocumentService;

@ExtendWith(SpringExtension.class)
public class DocumentServiceTest {

	private DocumentService documentService;

	@BeforeEach
	public void setUp() {
		documentService = new DocumentService();
	}

	@Test
	public void getDocumentTest() throws IOException {
		Document doc = documentService.getDocument("https://www.otomoto.pl/osobowe/alfa-romeo/brera/", "");
		assertEquals("Alfa Romeo Brera - samochody osobowe - otomoto.pl", doc.title());
	}
}
