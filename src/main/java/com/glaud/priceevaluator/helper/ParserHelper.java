package com.glaud.priceevaluator.helper;

import com.glaud.priceevaluator.model.FuelType;

public class ParserHelper {

	public static FuelType parseFuel(String fuelTypeText) {
		switch (fuelTypeText.toLowerCase()) {
		case "benzyna":
			return FuelType.GASOLINE;
		case "diesel":
			return FuelType.DIESEL;
		case "lpg":
			return FuelType.LPG;
		default:
			return FuelType.UNKNOWN;
		}
	}
}
