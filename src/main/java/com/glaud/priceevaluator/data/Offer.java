package com.glaud.priceevaluator.data;

import com.glaud.priceevaluator.model.FuelType;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Offer {

	private Offer() {
	}

	private Price price;
	private String prodYear;
	private Integer mileage;
	private Integer engineCapacity;
	private FuelType fuelType;

	public static class Builder {
		private Price price;
		private String prodYear;
		private Integer mileage;
		private Integer engineCapacity;
		private FuelType fuelType;

		public Builder(Price price) {
			this.price = price;
		}

		public Builder producedIn(String prodYear) {
			this.prodYear = prodYear;
			return this;
		}

		public Builder withMileage(Integer mileage) {
			this.mileage = mileage;
			return this;
		}

		public Builder withEngineCapacity(Integer engineCapacity) {
			this.engineCapacity = engineCapacity;
			return this;
		}

		public Builder withFuelType(FuelType fuelType) {
			this.fuelType = fuelType;
			return this;
		}

		public Offer build() {
			Offer offer = new Offer();
			offer.price = this.price;
			offer.prodYear = this.prodYear;
			offer.mileage = this.mileage;
			offer.engineCapacity = this.engineCapacity;
			offer.fuelType = this.fuelType;
			return offer;
		}

	}

}
