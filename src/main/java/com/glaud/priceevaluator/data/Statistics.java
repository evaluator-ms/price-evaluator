package com.glaud.priceevaluator.data;

import lombok.Data;

@Data
public class Statistics {

	private int count;
	private double avg;
	private double min;
	private double max;
	private PercentileRange percentileRange;
}
