package com.glaud.priceevaluator.data;

import com.glaud.priceevaluator.model.Currency;

import lombok.Data;

@Data
public class Price {

	public Price(double value, Currency currency) {
		this.value = value;
		this.currency = currency;
	}

	private double value;
	private Currency currency;

}
