package com.glaud.priceevaluator.data;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.glaud.priceevaluator.model.Currency;

import lombok.Data;

@Component
@Data
public class CurrencyRates {

	@Value("${rates-api.url:https://api.exchangeratesapi.io/latest?base=PLN&symbols=USD,EUR,GBP}")
	private String url;

	private Map<Currency, Double> currencyRates = new HashMap<>();
	private JSONObject jsonRates;

	@Autowired
	private RestTemplate restTemplate;

	public double getCurrencyRate(Currency currency) {
		return this.currencyRates.get(currency);
	}

	@Scheduled(cron = "1 0 7 * * ?")
	public void updateCurrencyRates() {
		String response = restTemplate.getForObject(url, String.class);
		JSONObject jObj = new JSONObject(response);
		this.jsonRates = ((JSONObject) jObj.get("rates"));
		currencyRates.put(Currency.EUR, parseCurrencyRate(Currency.EUR));
		currencyRates.put(Currency.USD, parseCurrencyRate(Currency.USD));
		currencyRates.put(Currency.GBP, parseCurrencyRate(Currency.GBP));
	}

	private Double parseCurrencyRate(Currency currency) {
		return jsonRates.getDouble(currency.toString());
	}

}
