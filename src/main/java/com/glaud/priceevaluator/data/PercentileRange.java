package com.glaud.priceevaluator.data;

import lombok.Data;

@Data
public class PercentileRange {

	private double upperRange;
	private double lowerRange;
}
