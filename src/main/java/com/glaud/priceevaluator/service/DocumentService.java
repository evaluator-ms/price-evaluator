package com.glaud.priceevaluator.service;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

@Service
public class DocumentService {

	public Document getDocument(String url, String query) throws IOException {
		Document doc = Jsoup.connect(url + query).timeout(25000).get();
		return doc;
	}

}
