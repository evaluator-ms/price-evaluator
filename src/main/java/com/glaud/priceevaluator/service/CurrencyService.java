package com.glaud.priceevaluator.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.glaud.priceevaluator.data.CurrencyRates;
import com.glaud.priceevaluator.data.Price;
import com.glaud.priceevaluator.model.Currency;

@Service
public class CurrencyService {

	private CurrencyRates currencyRates;

	@Autowired
	public CurrencyService(CurrencyRates currencyRates) {
		this.currencyRates = currencyRates;
	}
	
	public double convertPriceToPln(Price price) {
		Currency currency = price.getCurrency();
		if (currency.equals(Currency.PLN)) {
			return price.getValue();
		} else {
			double rate = currencyRates.getCurrencyRate(currency);
			return price.getValue() / rate;
		}
	}

}
