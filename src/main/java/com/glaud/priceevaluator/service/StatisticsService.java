package com.glaud.priceevaluator.service;

import java.io.IOException;
import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.glaud.priceevaluator.data.Offer;
import com.glaud.priceevaluator.data.Price;
import com.glaud.priceevaluator.data.Statistics;

@Service
public class StatisticsService {

	private OfferService offerService;
	private CurrencyService currencyService;

	@Autowired
	public StatisticsService(OfferService offerService, CurrencyService currencyService) {
		this.offerService = offerService;
		this.currencyService = currencyService;
	}

	public Statistics computeStatistics(String mark, String modelName) throws IOException {
		List<Offer> offers = offerService.getOfferList(mark, modelName);
		List<Price> prices = offers.stream().map(offer -> offer.getPrice()).collect(Collectors.toList());
		List<Double> pricesInPln = prices.stream().map(price -> currencyService.convertPriceToPln(price))
				.collect(Collectors.toList());
		Statistics statistics = new Statistics();
		statistics.setCount(offers.size());
		statistics.setAvg(computeAvg(pricesInPln));
		statistics.setMin(computeMin(pricesInPln));
		statistics.setMax(computeMax(pricesInPln));
		return statistics;
	}

	private double computeAvg(List<Double> prices) {
		OptionalDouble avg = prices.stream().mapToDouble(p -> p).average();
		return avg.getAsDouble();
	}

	private double computeMin(List<Double> prices) {
		OptionalDouble min = prices.stream().mapToDouble(p -> p).min();
		return min.getAsDouble();
	}

	private double computeMax(List<Double> prices) {
		OptionalDouble max = prices.stream().mapToDouble(p -> p).max();
		return max.getAsDouble();
	}

}
