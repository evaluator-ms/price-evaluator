package com.glaud.priceevaluator.service;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

import com.glaud.priceevaluator.config.RabbitConfig;
import com.glaud.priceevaluator.model.Mark;
import com.glaud.priceevaluator.model.MarkAndModel;
import com.glaud.priceevaluator.model.Model;

@Service
public class CarService {

	private RabbitTemplate rabbitTemplate;

	@Autowired
	public CarService(RabbitTemplate rabbitTemplate) {
		this.rabbitTemplate = rabbitTemplate;
	}

	public Mark getMark(String name) {
		Mark mark = rabbitTemplate.convertSendAndReceiveAsType(RabbitConfig.EXCHANGE_MODELS, "rpc.models", name,
				new ParameterizedTypeReference<Mark>() {
				});
		System.out.println("Received and converted response: " + mark);
		return mark;
	}

	public Model getModel(String markName, String modelName) {
		Model model = rabbitTemplate.convertSendAndReceiveAsType(RabbitConfig.EXCHANGE_MODELS, "rpc.models.info",
				new MarkAndModel(markName, modelName), new ParameterizedTypeReference<Model>() {
				});
		System.out.println("Received and converted response: " + model);
		return model;
	}

}
