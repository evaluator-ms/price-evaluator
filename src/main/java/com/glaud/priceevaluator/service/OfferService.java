package com.glaud.priceevaluator.service;

import java.io.IOException;
import java.util.List;

import com.glaud.priceevaluator.data.Offer;

public interface OfferService {

	List<Offer> getOfferList(String mark, String model) throws IOException;

}
