package com.glaud.priceevaluator.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.glaud.priceevaluator.data.Offer;
import com.glaud.priceevaluator.data.Price;
import com.glaud.priceevaluator.helper.ParserHelper;
import com.glaud.priceevaluator.model.Currency;
import com.glaud.priceevaluator.model.FuelType;

@Service
public class OtoMotoOfferService implements OfferService {

	@Value("${otomoto.url:https://www.otomoto.pl/osobowe/}")
	private String url;

	private DocumentService documentService;

	@Autowired
	public OtoMotoOfferService(DocumentService documentService) {
		this.documentService = documentService;
	}

	@Override
	public List<Offer> getOfferList(String mark, String model) throws IOException {
		Elements offersArticles = getAllOffersArticles(mark, model);
		List<Offer> offers = createOffers(offersArticles);
		return offers;
	}

	public Elements getAllOffersArticles(String mark, String model) throws IOException {
		String query = mark + "/" + model;
		Document doc = documentService.getDocument(url, query);
		int pagesCount = findPagesCount(doc);
		Elements offerList = doc.getElementsByTag("article");
		if (pagesCount > 1) {
			offerList.addAll(getRemainingOfferList(query, pagesCount));
		}
		return offerList;
	}

	private int findPagesCount(Document doc) {
		int offersCount = findOffersCount(doc);
		return (int) Math.ceil((double) offersCount / (double) 32);
	}

	private int findOffersCount(Document doc) {
		Elements spans = doc.getElementsByTag("span");
		List<Element> filteredSpans = spans.stream().filter(span -> span.hasClass("counter"))
				.collect(Collectors.toList());
		int offersCount = 0;
		if (!filteredSpans.isEmpty()) {
			offersCount = Integer.parseInt(filteredSpans.get(0).text().replaceAll("[()\\s]", ""));
		}
		return offersCount;
	}

	private Elements getRemainingOfferList(String query, int pagesCount) throws IOException {
		Elements remainingOfferList = new Elements();
		for (int i = 2; i <= pagesCount; i++) {
			Document doc = documentService.getDocument(url, query + "?page=" + i);
			remainingOfferList.addAll(doc.getElementsByTag("article"));
		}
		return remainingOfferList;
	}

	public List<Offer> createOffers(Elements offersArticles) {
		List<Offer> offers = new ArrayList<>();
		offersArticles.forEach(offerArticle -> {
			Offer offer = new Offer.Builder(findPrice(offerArticle)).producedIn(findProdYear(offerArticle))
					.withEngineCapacity(findEngineCapacity(offerArticle)).withMileage(findMileage(offerArticle))
					.withFuelType(findFuelType(offerArticle)).build();
			offers.add(offer);
		});
		return offers;
	}

	private Price findPrice(Element offerArticle) {
		String offerText = offerArticle.getElementsByClass("offer-price__number").text();
		double value = Double
				.parseDouble(offerText.substring(0, offerText.length() - 3).replace(" ", "").replace(",", "."));
		Currency currency = Currency.valueOf(offerText.substring(offerText.length() - 3));
		return new Price(value, currency);
	}

	private String findProdYear(Element offerArticle) {
		Element yearElement = offerArticle.getElementsByAttributeValue("data-code", "year").first();
		if (yearElement == null) {
			return null;
		}
		return yearElement.getElementsByTag("span").text();
	}

	private Integer findEngineCapacity(Element offerArticle) {
		Element engineCapacityElement = offerArticle.getElementsByAttributeValue("data-code", "engine_capacity")
				.first();
		if (engineCapacityElement == null) {
			return null;
		}
		String engineCapacityText = engineCapacityElement.getElementsByTag("span").text();
		return Integer.parseInt(engineCapacityText.substring(0, engineCapacityText.length() - 3).replace(" ", ""));
	}

	private Integer findMileage(Element offerArticle) {
		Element mileageElement = offerArticle.getElementsByAttributeValue("data-code", "mileage").first();
		if (mileageElement == null) {
			return null;
		}
		String mileageText = mileageElement.getElementsByTag("span").text();
		return Integer.parseInt(mileageText.substring(0, mileageText.length() - 3).replace(" ", ""));
	}

	private FuelType findFuelType(Element offerArticle) {
		Element fuelElement = offerArticle.getElementsByAttributeValue("data-code", "fuel_type").first();
		if (fuelElement == null) {
			return FuelType.UNKNOWN;
		}
		String fuelTypeText = fuelElement.getElementsByTag("span").text();
		return ParserHelper.parseFuel(fuelTypeText);
	}
}
