package com.glaud.priceevaluator;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.glaud.priceevaluator.config.AppConfig;
import com.glaud.priceevaluator.config.RabbitConfig;
import com.glaud.priceevaluator.data.CurrencyRates;

@SpringBootApplication
@Import({ AppConfig.class, RabbitConfig.class })
@EnableScheduling
@EnableEurekaClient
public class PriceEvaluatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(PriceEvaluatorApplication.class, args);
	}

	@Bean
	CommandLineRunner init(CurrencyRates currencyRates) {
		return (args) -> {
			currencyRates.updateCurrencyRates();
		};
	}
}
