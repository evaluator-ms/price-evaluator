package com.glaud.priceevaluator.model;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Detail {
	
	public static Detail NOT_FOUND = new Detail.Builder("Details not found").build();

	private String name;
	private String prodYears;
	private String rate;

	public static class Builder {
		private String name;
		private String prodYears;
		private String rate;

		public Builder(String name) {
			this.name = name;
		}

		public Builder produced(String prodYears) {
			this.prodYears = prodYears;
			return this;
		}

		public Builder withRate(String rate) {
			this.rate = rate;
			return this;
		}

		public Detail build() {
			Detail detail = new Detail();
			detail.name = this.name;
			detail.prodYears = this.prodYears;
			detail.rate = this.rate;
			return detail;
		}
	}

	private Detail() {
	}

}
