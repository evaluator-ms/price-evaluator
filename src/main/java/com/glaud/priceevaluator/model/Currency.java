package com.glaud.priceevaluator.model;

public enum Currency {

	EUR, USD, GBP, PLN
	
}
