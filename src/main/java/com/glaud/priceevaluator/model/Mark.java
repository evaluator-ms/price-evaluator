package com.glaud.priceevaluator.model;

import java.util.List;

import lombok.Data;

@Data
public class Mark {

	private String id;
	private String name;
	private String rate;
	private String originCountry;
	private String priceRange;
	private List<Model> models;

	public static class Builder {
		private String name;
		private String rate;
		private String originCountry;
		private String priceRange;
		private List<Model> models;

		public Builder(String name) {
			this.name = name;
		}

		public Builder withModels(List<Model> models) {
			this.models = models;
			return this;
		}

		public Builder withRate(String rate) {
			this.rate = rate;
			return this;
		}

		public Builder from(String originCountry) {
			this.originCountry = originCountry;
			return this;
		}

		public Builder withPriceRange(String priceRange) {
			this.priceRange = priceRange.substring(0, priceRange.length() - 4);
			return this;
		}

		public Mark build() {
			Mark mark = new Mark();
			mark.name = this.name;
			mark.models = this.models;
			mark.rate = this.rate;
			mark.originCountry = this.originCountry;
			mark.priceRange = this.priceRange;
			return mark;
		}
	}

	private Mark() {
	}

}
