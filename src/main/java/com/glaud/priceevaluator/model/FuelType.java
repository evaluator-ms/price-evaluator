package com.glaud.priceevaluator.model;

public enum FuelType {
	GASOLINE, DIESEL, LPG, UNKNOWN

}
