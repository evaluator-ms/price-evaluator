package com.glaud.priceevaluator.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Model {

	private String name;
	private String prodYears;
	private String rate;
	private String priceStartingAt;
	private List<Detail> details;

	public static class Builder {
		private String name;
		private String prodYears;
		private String rate;
		private String priceStartingAt;
		private List<Detail> details;

		public Builder(String name) {
			this.name = name;
		}

		public Builder withDetails(List<Detail> details) {
			this.details = details;
			return this;
		}

		public Builder produced(String prodYears) {
			this.prodYears = prodYears;
			return this;
		}
		
		public Builder withRate(String rate) {
			this.rate = rate;
			return this;
		}
		
		public Builder withPriceStartingAt(String price) {
			this.priceStartingAt = price;
			return this;
		}

		public Model build() {
			Model model = new Model();
			model.name = this.name;
			model.prodYears = this.prodYears;
			model.rate = this.rate;
			model.details = this.details;
			model.priceStartingAt = this.priceStartingAt;
			return model;
		}
	}

	private Model() {
	}
}
