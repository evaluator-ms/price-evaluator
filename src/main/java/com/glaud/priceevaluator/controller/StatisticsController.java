package com.glaud.priceevaluator.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.glaud.priceevaluator.data.Statistics;
import com.glaud.priceevaluator.service.StatisticsService;

@Controller
public class StatisticsController {

	private StatisticsService statisticsService;

	@Autowired
	public StatisticsController(StatisticsService statisticsService) {
		this.statisticsService = statisticsService;
	}

	@GetMapping("/getStatistics")
	@ResponseBody
	public Statistics getStatistics(@RequestParam("mark") String mark, @RequestParam("model") String modelName,
			Model model) throws IOException {
			return statisticsService.computeStatistics(mark, modelName);
	}

}
