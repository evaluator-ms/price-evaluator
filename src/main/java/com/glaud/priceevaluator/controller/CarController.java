package com.glaud.priceevaluator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.glaud.priceevaluator.model.Mark;
import com.glaud.priceevaluator.model.Model;
import com.glaud.priceevaluator.service.CarService;

@Controller
public class CarController {

	@Autowired
	private CarService carService;

	@GetMapping(value = "/getModels")
	@ResponseBody
	public Mark getModels(@RequestParam("mark") String mark) {
		Mark car = carService.getMark(mark);
		return car;
	}

	@GetMapping(value = "/getModelInfo")
	@ResponseBody
	public Model getModelInfo(@RequestParam("mark") String markName, @RequestParam("model") String modelName) {
		Model model = carService.getModel(markName, modelName);
		return model;
	}

}
